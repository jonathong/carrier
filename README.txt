STRUCTURE:
1. App module is the presentation/UI layer. This layer is in charge of presenting data to the user and manipulating UI elements.
- This module communicates to the domain layer and passes data controllers to use cases (or interactors).
2. Domain module is the core of the application.
- Houses business logic. Generally, you could say that all other layers are just plumbing.
- Requirement changes are often isolated to this layer.
- The domain should not know of the other layers (besides common)
3. Repository (data) module is in charge of receiving/transmitting data.
- Not much to say about this module. You give it data or it gives you data.

TODO:
There is some clean up that needs to occur. First, DI of some sort should be introduced. Wrapping of results from usecases and the repository would be a good addition as well.