package com.example.carrier

import android.app.Application
import io.realm.Realm
import timber.log.Timber

class App : Application(){
    override fun onCreate() {
        super.onCreate()
        setupLogging()
        Realm.init(this)
    }

    private fun setupLogging(){
        if(BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}