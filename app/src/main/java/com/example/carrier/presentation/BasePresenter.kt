package com.example.carrier.presentation

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

abstract class BasePresenter(private val uiContext: CoroutineContext = Dispatchers.Main)
    : CoroutineScope {

    private var job: Job = Job()
    override val coroutineContext: CoroutineContext
        get() = uiContext + job

    fun onUIDestroyed(){
        job.cancel()
    }
}