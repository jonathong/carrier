package com.example.carrier.presentation

import com.example.carrier.domain.data.model.ShiftData
import com.example.carrier.domain.usecase.GetShiftDataUseCase
import com.example.carrier.domain.usecase.SendShiftMessageUseCase
import com.example.carrier.domain.usecase.impl.GetShiftDataUseCaseImpl
import com.example.carrier.domain.usecase.impl.SendShiftMessageUseCaseImpl
import com.example.carrier.repository.ShiftRepository
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class ShiftDetailsPresenter(
    private val view: ShiftDetailsView,
    private val repo: ShiftRepository = ShiftRepository.getInstance(),
    private val shiftDataUseCase: GetShiftDataUseCase = GetShiftDataUseCaseImpl(repo, repo),
    private val sendMessageUseCase: SendShiftMessageUseCase = SendShiftMessageUseCaseImpl(repo)
) : BasePresenter(){


    fun onUICreated() = launch {
        view.initializeUIElements()
        val data = async(Dispatchers.IO){
            shiftDataUseCase.execute(1)
        }
        view.refreshUI(data.await())
    }

    fun onSendMessageClicked(message: String) = launch {
        val result = async(Dispatchers.IO){
            sendMessageUseCase.execute(1, message)
        }
        view.showToast(result.await().name)
    }

    interface ShiftDetailsView{
        fun initializeUIElements()
        fun refreshUI(data: ShiftData)
        fun showToast(message: String)
    }
}