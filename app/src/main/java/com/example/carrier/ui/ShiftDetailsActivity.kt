package com.example.carrier.ui

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.carrier.R
import com.example.carrier.domain.data.model.ShiftData
import com.example.carrier.presentation.ShiftDetailsPresenter

class ShiftDetailsActivity : AppCompatActivity(), ShiftDetailsPresenter.ShiftDetailsView {
    private lateinit var statusView: TextView
    private lateinit var driverIdView: TextView
    private lateinit var driverNameView: TextView
    private lateinit var sendHelloButton: Button

    private val presenter = ShiftDetailsPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onUICreated()
    }

    override fun onDestroy() {
        presenter.onUIDestroyed()
        super.onDestroy()
    }

    override fun initializeUIElements() {
        setContentView(R.layout.activity_shift_details)
        statusView = findViewById(R.id.activity_shift_details_status)
        driverIdView = findViewById(R.id.activity_shift_details_driver_id)
        driverNameView = findViewById(R.id.activity_shift_details_driver_name)
        sendHelloButton = findViewById(R.id.activity_shift_details_button_hello)

        sendHelloButton.setOnClickListener { presenter.onSendMessageClicked("Hello World!") }
    }

    override fun refreshUI(data: ShiftData){
        statusView.text = data.status.statusName
        driverIdView.text = data.driverId.toString()
        driverNameView.text = data.driverName
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}
