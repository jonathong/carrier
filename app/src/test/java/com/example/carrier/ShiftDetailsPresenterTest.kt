package com.example.carrier

import com.example.carrier.domain.data.model.MessageResult
import com.example.carrier.domain.data.model.ShiftData
import com.example.carrier.domain.data.source.DetailsDataSource
import com.example.carrier.domain.data.source.MessageDataSource
import com.example.carrier.domain.data.source.StatusDataSource
import com.example.carrier.domain.usecase.SendShiftMessageUseCase
import com.example.carrier.presentation.ShiftDetailsPresenter
import com.example.carrier.repository.ShiftRepository
import com.google.common.truth.Truth
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

class ShiftDetailsPresenterTest{
    lateinit var view: ShiftDetailsPresenter.ShiftDetailsView
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @Before
    fun setup(){
        Dispatchers.setMain(mainThreadSurrogate)
        view = mock(ShiftDetailsPresenter.ShiftDetailsView::class.java)
    }

    @Test
    fun testOnUICreated() = runBlockingTest {
        val fakeRepo = ShiftRepository(localStatusDataSource = object : StatusDataSource {
            override suspend fun getShiftStatus(id: Int): ShiftData.Status = ShiftData.Status.STARTED
        }, remoteDetailsDataSource = object : DetailsDataSource {
            override suspend fun getShiftData(id: Int): ShiftData
                    = ShiftData(0, ShiftData.Status.SCHEDULED, 1, "Joe Trucker")
        }, remoteMessageDataSource = object : MessageDataSource{
            override suspend fun sendShiftMessage(id: Int, message: String): MessageResult {
                TODO("Not yet implemented")
            }
        })
        val presenter = ShiftDetailsPresenter(view, fakeRepo)
        presenter.onUICreated()
        presenter.launch {
            presenter.onUICreated()
            verify(view).initializeUIElements()
            verify(view).refreshUI(ShiftData(0, ShiftData.Status.STARTED, 1, "Joe Trucker"))
        }
    }

    @Test
    fun testOnSendMessage() = runBlockingTest {
        val presenter = ShiftDetailsPresenter(view, sendMessageUseCase = object: SendShiftMessageUseCase{
            override val remoteSource: MessageDataSource
                get() = TODO("Not yet implemented")
            override suspend fun execute(id: Int, message: String): MessageResult {
                Truth.assertThat(message).isEqualTo("Blah")
                return MessageResult.OK
            }
        })
        presenter.launch {
            presenter.onSendMessageClicked("Blah")
            verify(view).showToast(MessageResult.OK.name)
        }
    }
}