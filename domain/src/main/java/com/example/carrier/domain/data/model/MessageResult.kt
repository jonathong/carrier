package com.example.carrier.domain.data.model

enum class MessageResult{
    OK, ERROR
}