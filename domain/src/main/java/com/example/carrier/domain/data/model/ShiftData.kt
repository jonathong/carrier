package com.example.carrier.domain.data.model

data class ShiftData(val id: Int, val status: Status, val driverId: Int, val driverName: String){

    enum class Status(val statusValue: Int, val statusName: String){
        NOT_FOUND(-1, "not found"),
        SCHEDULED(0, "scheduled"),
        STARTED(1, "started"),
        FINISHED(2, "finished");

        companion object {
            fun fromValue(statusValue: Int): Status {
                values().forEach { if (it.statusValue == statusValue) return it }
                return NOT_FOUND
            }

            fun fromName(statusName: String): Status {
                values().forEach { if (it.statusName == statusName) return it }
                return NOT_FOUND
            }
        }
    }
}