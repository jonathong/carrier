package com.example.carrier.domain.data.source

import com.example.carrier.domain.data.model.ShiftData

interface DetailsDataSource {
    suspend fun getShiftData(id: Int): ShiftData
}