package com.example.carrier.domain.data.source

import com.example.carrier.domain.data.model.MessageResult

interface MessageDataSource {
    suspend fun sendShiftMessage(id: Int, message: String): MessageResult
}