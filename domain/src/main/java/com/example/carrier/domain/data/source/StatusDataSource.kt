package com.example.carrier.domain.data.source

import com.example.carrier.domain.data.model.ShiftData

interface StatusDataSource {
    suspend fun getShiftStatus(id: Int): ShiftData.Status
}