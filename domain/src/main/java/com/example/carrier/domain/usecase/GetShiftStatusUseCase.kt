package com.example.carrier.domain.usecase

import com.example.carrier.domain.data.model.ShiftData
import com.example.carrier.domain.data.source.DetailsDataSource
import com.example.carrier.domain.data.source.StatusDataSource

/**
 * Use case for receiving the status of a given shift. Implementation is found under "impl" package.
 * - Might make more sense to return a wrapped Result<T> that may also return an error to the UI layer.
 */
interface GetShiftDataUseCase{
    val localStatusSource: StatusDataSource
    val dataSource: DetailsDataSource

    suspend fun execute(id: Int): ShiftData
}