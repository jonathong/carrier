package com.example.carrier.domain.usecase

import com.example.carrier.domain.data.model.MessageResult
import com.example.carrier.domain.data.source.MessageDataSource

/**
 * UseCase interface for sending a message to a particular shift. Implementation of this interface
 * is found under "impl" package.
 */
interface SendShiftMessageUseCase{
    val remoteSource: MessageDataSource

    suspend fun execute(id: Int, message: String): MessageResult
}