package com.example.carrier.domain.usecase.impl

import com.example.carrier.domain.data.model.ShiftData
import com.example.carrier.domain.data.source.DetailsDataSource
import com.example.carrier.domain.data.source.StatusDataSource
import com.example.carrier.domain.usecase.GetShiftDataUseCase

class GetShiftDataUseCaseImpl(
    override val localStatusSource: StatusDataSource,
    override val dataSource: DetailsDataSource) : GetShiftDataUseCase {

    override suspend fun execute(id: Int): ShiftData {
        val localShiftStatus = localStatusSource.getShiftStatus(id)
        val remoteShiftData = dataSource.getShiftData(id)
        if(localShiftStatus != ShiftData.Status.NOT_FOUND){
            return ShiftData(id, localShiftStatus, remoteShiftData.driverId, remoteShiftData.driverName)
        }
        return remoteShiftData
    }
}