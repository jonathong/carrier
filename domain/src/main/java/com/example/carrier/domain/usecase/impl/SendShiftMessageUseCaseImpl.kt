package com.example.carrier.domain.usecase.impl

import com.example.carrier.domain.data.model.MessageResult
import com.example.carrier.domain.data.source.MessageDataSource
import com.example.carrier.domain.usecase.SendShiftMessageUseCase

class SendShiftMessageUseCaseImpl(
    override val remoteSource: MessageDataSource) : SendShiftMessageUseCase{

    override suspend fun execute(id: Int, message: String,): MessageResult {
        return remoteSource.sendShiftMessage(id, message)
    }
}