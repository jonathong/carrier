package com.example.carrier.domain

import com.example.carrier.domain.data.model.ShiftData
import com.example.carrier.domain.data.source.DetailsDataSource
import com.example.carrier.domain.data.source.StatusDataSource
import com.example.carrier.domain.usecase.impl.GetShiftDataUseCaseImpl
import com.google.common.truth.Truth
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

class GetShiftStatusUseCaseTest {
    @Test
    fun testExecute_local_wins() = runBlockingTest{
        val usecase = GetShiftDataUseCaseImpl(object: StatusDataSource{
            override suspend fun getShiftStatus(id: Int): ShiftData.Status = ShiftData.Status.STARTED
        }, object: DetailsDataSource{
            override suspend fun getShiftData(id: Int): ShiftData = ShiftData(0, ShiftData.Status.SCHEDULED, 1, "Joe Trucker")
        })
        Truth.assertThat(usecase.execute(0)).isEqualTo(
            ShiftData(0, ShiftData.Status.STARTED, 1, "Joe Trucker")
        )
    }

    @Test
    fun testExecute_local_same() = runBlockingTest{
        val usecase = GetShiftDataUseCaseImpl(object: StatusDataSource{
            override suspend fun getShiftStatus(id: Int): ShiftData.Status
                    = ShiftData.Status.SCHEDULED
        }, object: DetailsDataSource{
            override suspend fun getShiftData(id: Int): ShiftData
                    = ShiftData(0, ShiftData.Status.SCHEDULED, 1, "Joe Trucker")
        })
        Truth.assertThat(usecase.execute(0)).isEqualTo(
            ShiftData(0, ShiftData.Status.SCHEDULED, 1, "Joe Trucker")
        )
    }

    @Test
    fun testExecute_local_error() = runBlockingTest{
        val usecase = GetShiftDataUseCaseImpl(object: StatusDataSource{
            override suspend fun getShiftStatus(id: Int): ShiftData.Status
                    = ShiftData.Status.NOT_FOUND
        }, object: DetailsDataSource{
            override suspend fun getShiftData(id: Int): ShiftData
                    = ShiftData(0, ShiftData.Status.SCHEDULED, 1, "Joe Trucker")
        })
        Truth.assertThat(usecase.execute(0)).isEqualTo(
            ShiftData(0, ShiftData.Status.SCHEDULED, 1, "Joe Trucker")
        )
    }
}