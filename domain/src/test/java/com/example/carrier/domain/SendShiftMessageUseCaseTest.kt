package com.example.carrier.domain

import com.example.carrier.domain.data.model.MessageResult
import com.example.carrier.domain.data.source.MessageDataSource
import com.example.carrier.domain.usecase.SendShiftMessageUseCase
import com.example.carrier.domain.usecase.impl.SendShiftMessageUseCaseImpl
import com.google.common.truth.Truth
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

class SendShiftMessageUseCaseTest {
    @Test
    fun testExecute() = runBlockingTest{
        val messageVal = "blah"
        val idVal = 3
        val usecase = SendShiftMessageUseCaseImpl(object : MessageDataSource{
            override suspend fun sendShiftMessage(id: Int, message: String): MessageResult {
                Truth.assertThat(message).isEqualTo(messageVal)
                Truth.assertThat(id).isEqualTo(idVal)
                return MessageResult.OK
            }
        })
        val usecaseResult = usecase.execute(idVal, messageVal)
        Truth.assertThat(usecaseResult).isEqualTo(MessageResult.OK)
    }
    @Test
    fun testExecute_error() = runBlockingTest{
        val usecase = SendShiftMessageUseCaseImpl(object : MessageDataSource{
            override suspend fun sendShiftMessage(id: Int, message: String): MessageResult {
                return MessageResult.ERROR
            }
        })
        val usecaseResult = usecase.execute(0, "")
        Truth.assertThat(usecaseResult).isEqualTo(MessageResult.ERROR)
    }
}