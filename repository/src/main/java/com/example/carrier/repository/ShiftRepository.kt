package com.example.carrier.repository

import com.example.carrier.domain.data.source.MessageDataSource
import com.example.carrier.domain.data.source.DetailsDataSource
import com.example.carrier.domain.data.model.MessageResult
import com.example.carrier.domain.data.model.ShiftData
import com.example.carrier.domain.data.source.StatusDataSource
import com.example.carrier.repository.local.DetailsLocalSource
import com.example.carrier.repository.remote.DetailsRemoteSource
import com.example.carrier.repository.remote.MessageRemoteSource

/**
 * Repository class that determines where data is retrieved from local or remote sources. This repository
 * retrieves data about a shift and processes the sending of messages to a shift.
 */
open class ShiftRepository(
    private val localStatusDataSource: StatusDataSource,
    private val remoteDetailsDataSource: DetailsDataSource,
    private val remoteMessageDataSource: MessageDataSource
) : DetailsDataSource, MessageDataSource, StatusDataSource {

    companion object {
        private var INSTANCE: ShiftRepository? = null
        // TODO(Replace with hilt/dagger)
        fun getInstance(): ShiftRepository {
            if (INSTANCE == null) {
                INSTANCE = ShiftRepository(DetailsLocalSource(), DetailsRemoteSource(), MessageRemoteSource())
            }
            return INSTANCE!!
        }
    }

    override suspend fun getShiftStatus(id: Int): ShiftData.Status {
        return localStatusDataSource.getShiftStatus(id)
    }

    override suspend fun getShiftData(id: Int): ShiftData {
        return remoteDetailsDataSource.getShiftData(id)
    }

    override suspend fun sendShiftMessage(id: Int, message: String): MessageResult {
        return remoteMessageDataSource.sendShiftMessage(id, message)
    }
}