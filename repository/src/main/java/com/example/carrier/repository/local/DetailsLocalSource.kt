package com.example.carrier.repository.local

import com.example.carrier.domain.data.source.DetailsDataSource
import com.example.carrier.domain.data.model.ShiftData
import com.example.carrier.domain.data.source.StatusDataSource
import com.example.carrier.repository.local.model.ShiftLocalModel
import io.realm.Realm

/**
 * Local source of shift detail data. Realm is used for simplicity's sake - however, it should be replaced
 * in the future with Room.
 *
 * When Realm is replaced with Room, only files under the "local" package will need to change.
 */
internal class DetailsLocalSource(): StatusDataSource {
    val ID = "id"

    override suspend fun getShiftStatus(id: Int): ShiftData.Status {
        val status = Realm.getDefaultInstance().use{
            it.where(ShiftLocalModel::class.java)
                .equalTo(ID, id)
                .findFirst()?.status ?: ShiftData.Status.NOT_FOUND.name
        }
        return ShiftData.Status.fromName(status)
    }
}

