package com.example.carrier.repository.local.model

import com.example.carrier.domain.data.model.ShiftData
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class ShiftLocalModel() : RealmObject(){
    @PrimaryKey var id: Int = 0
    var status: String = ShiftData.Status.NOT_FOUND.name
}