package com.example.carrier.repository.remote

import com.example.carrier.domain.data.source.DetailsDataSource
import com.example.carrier.domain.data.model.ShiftData
import com.example.carrier.repository.remote.retrofit.CarrierRetrofit
import com.example.carrier.repository.remote.retrofit.definitions.ShiftService

internal class DetailsRemoteSource() :
    DetailsDataSource {
    private val service: ShiftService = CarrierRetrofit.getShiftService()

    override suspend fun getShiftData(id: Int): ShiftData {
        /*val remoteData = service.getShiftDetails(id)
        return ShiftData(
            remoteData.id,
            ShiftData.Status.fromName(remoteData.status),
            remoteData.driverId,
            remoteData.driverName
        )*/
        return ShiftData(
            1,
            ShiftData.Status.SCHEDULED,
            2,
            "Joe Trucker"
        )
    }
}