package com.example.carrier.repository.remote

import com.example.carrier.domain.data.source.MessageDataSource
import com.example.carrier.domain.data.model.MessageResult
import com.example.carrier.repository.remote.model.MessageRemoteBody
import com.example.carrier.repository.remote.retrofit.CarrierRetrofit
import com.example.carrier.repository.remote.retrofit.definitions.ShiftService
import timber.log.Timber
import java.lang.Exception

internal class MessageRemoteSource : MessageDataSource {
    private val service: ShiftService = CarrierRetrofit.getShiftService()

    override suspend fun sendShiftMessage(id: Int, message: String): MessageResult {
        try {
            val response = service.postMessage(id, MessageRemoteBody(message))
            if(response.isSuccessful) return MessageResult.OK
        }catch (e: Exception){
            Timber.e(e)
        }
        return MessageResult.ERROR
    }
}