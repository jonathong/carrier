package com.example.carrier.repository.remote.model

import com.google.gson.annotations.SerializedName

internal data class DetailsRemoteModel(
    @SerializedName("id")
    val id: Int,
    @SerializedName("status")
    val status: String,
    @SerializedName("driver_id")
    val driverId: Int,
    @SerializedName("driver_name")
    val driverName: String
)