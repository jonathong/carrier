package com.example.carrier.repository.remote.model

import com.google.gson.annotations.SerializedName

internal data class MessageRemoteBody(
    @SerializedName("message")
    val message: String
)