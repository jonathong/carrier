package com.example.carrier.repository.remote.retrofit

import com.example.carrier.repository.remote.retrofit.definitions.ShiftService
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

// Class needs some clean up. A bit messy right now.
internal object CarrierRetrofit{
    private val PROD_URL = "https://example.haulhub.com"

    private val httpClientBuilder = getHttpClientBuilder()
    private fun getHttpClientBuilder(): OkHttpClient.Builder{
        val builder = OkHttpClient.Builder()
            .connectTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES)
            .readTimeout(1, TimeUnit.MINUTES)
        return builder
    }

    private fun getDefaultBuilder(okHttpClient: OkHttpClient): Retrofit.Builder {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
    }

    private fun <T> createService(service: Class<T>, url: String): T {
        Timber.i("Returning service (${service.simpleName}) pointing to $url")
        val client = httpClientBuilder.build()

        return getDefaultBuilder(client)
            .baseUrl(url)
            .build()
            .create(service)
    }

    // This needs to be cleaned up. Should save this for later use.
    internal fun getShiftService(): ShiftService
            = createService(ShiftService::class.java, PROD_URL)
}