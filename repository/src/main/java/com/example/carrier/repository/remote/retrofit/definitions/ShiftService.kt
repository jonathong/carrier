package com.example.carrier.repository.remote.retrofit.definitions

import com.example.carrier.repository.remote.model.MessageRemoteBody
import com.example.carrier.repository.remote.model.DetailsRemoteModel
import retrofit2.Response
import retrofit2.http.*

internal interface ShiftService {

    @POST("shifts/{id}/messages")
    suspend fun postMessage(
        @Path("id") id: Int,
        @Body request: MessageRemoteBody): Response<Unit>

    @DELETE("shifts/{id}")
    suspend fun getShiftDetails(
        @Path("id") id: Int
    ): DetailsRemoteModel

}